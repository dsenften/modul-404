using System;
using System.Windows.Forms;

namespace pingpong
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources =
                new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.GameField = new System.Windows.Forms.Panel();
            this.RightPaddle = new System.Windows.Forms.PictureBox();
            this.Ball = new System.Windows.Forms.PictureBox();
            this.StartGame = new System.Windows.Forms.Button();
            this.GameTimer = new System.Windows.Forms.Timer(this.components);
            this.ScrollRightPaddle = new System.Windows.Forms.VScrollBar();
            this.label1 = new System.Windows.Forms.Label();
            this.ShowPoints = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.MoveUp = new System.Windows.Forms.Button();
            this.MoveDown = new System.Windows.Forms.Button();
            this.MoveLeft = new System.Windows.Forms.Button();
            this.MoveRight = new System.Windows.Forms.Button();
            this.ResetGame = new System.Windows.Forms.Button();
            this.SelectControls = new System.Windows.Forms.GroupBox();
            this.PaddleControl = new System.Windows.Forms.RadioButton();
            this.BallControl = new System.Windows.Forms.RadioButton();
            this.GameField.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.RightPaddle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.Ball)).BeginInit();
            this.SelectControls.SuspendLayout();
            this.SuspendLayout();
            // 
            // GameField
            // 
            this.GameField.BackColor = System.Drawing.Color.SeaGreen;
            this.GameField.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.GameField.Controls.Add(this.RightPaddle);
            this.GameField.Controls.Add(this.Ball);
            this.GameField.Location = new System.Drawing.Point(15, 16);
            this.GameField.Name = "GameField";
            this.GameField.Size = new System.Drawing.Size(542, 324);
            this.GameField.TabIndex = 0;
            // 
            // RightPaddle
            // 
            this.RightPaddle.BackColor = System.Drawing.Color.Black;
            this.RightPaddle.Location = new System.Drawing.Point(533, 3);
            this.RightPaddle.Name = "RightPaddle";
            this.RightPaddle.Size = new System.Drawing.Size(3, 40);
            this.RightPaddle.TabIndex = 1;
            this.RightPaddle.TabStop = false;
            // 
            // Ball
            // 
            this.Ball.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.Ball.Location = new System.Drawing.Point(251, 148);
            this.Ball.Name = "Ball";
            this.Ball.Size = new System.Drawing.Size(24, 25);
            this.Ball.TabIndex = 0;
            this.Ball.TabStop = false;
            // 
            // StartGame
            // 
            this.StartGame.Location = new System.Drawing.Point(12, 413);
            this.StartGame.Name = "StartGame";
            this.StartGame.Size = new System.Drawing.Size(83, 48);
            this.StartGame.TabIndex = 1;
            this.StartGame.Text = "Spiel starten";
            this.StartGame.UseVisualStyleBackColor = true;
            this.StartGame.Click += new System.EventHandler(this.StartGame_Click);
            // 
            // GameTimer
            // 
            this.GameTimer.Interval = 40;
            this.GameTimer.Tick += new System.EventHandler(this.GameTimer_Tick);
            // 
            // ScrollRightPaddle
            // 
            this.ScrollRightPaddle.Location = new System.Drawing.Point(556, 16);
            this.ScrollRightPaddle.Name = "ScrollRightPaddle";
            this.ScrollRightPaddle.Size = new System.Drawing.Size(17, 323);
            this.ScrollRightPaddle.TabIndex = 2;
            this.ScrollRightPaddle.Value = 50;
            this.ScrollRightPaddle.Scroll += new System.Windows.Forms.ScrollEventHandler(this.ScrollRightPaddle_Scroll);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.label1.Location = new System.Drawing.Point(12, 366);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 23);
            this.label1.TabIndex = 3;
            this.label1.Text = "Punkte:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ShowPoints
            // 
            this.ShowPoints.Location = new System.Drawing.Point(101, 366);
            this.ShowPoints.Name = "ShowPoints";
            this.ShowPoints.Size = new System.Drawing.Size(82, 23);
            this.ShowPoints.TabIndex = 4;
            this.ShowPoints.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(194, 353);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(379, 125);
            this.label2.TabIndex = 5;
            this.label2.Text =
                "Tastatursteuerung:\r\n\r\nTaste\r\n\r\nH    Horizontale Flugrichtung umkehren\r\nV     Vert" +
                "ikale Flugrichtung umkehren\r\nP     Spiel pausieren\r\nS     Spiel weiterlaufen las" + "sen";
            // 
            // MoveUp
            // 
            this.MoveUp.BackColor = System.Drawing.Color.White;
            this.MoveUp.Image = ((System.Drawing.Image) (resources.GetObject("MoveUp.Image")));
            this.MoveUp.Location = new System.Drawing.Point(668, 114);
            this.MoveUp.Name = "MoveUp";
            this.MoveUp.Size = new System.Drawing.Size(45, 45);
            this.MoveUp.TabIndex = 6;
            this.MoveUp.Tag = "mUp";
            this.MoveUp.UseVisualStyleBackColor = false;
            this.MoveUp.Click += new System.EventHandler(this.MoveBall);
            // 
            // MoveDown
            // 
            this.MoveDown.BackColor = System.Drawing.Color.White;
            this.MoveDown.Image = ((System.Drawing.Image) (resources.GetObject("MoveDown.Image")));
            this.MoveDown.Location = new System.Drawing.Point(668, 216);
            this.MoveDown.Name = "MoveDown";
            this.MoveDown.Size = new System.Drawing.Size(45, 45);
            this.MoveDown.TabIndex = 7;
            this.MoveDown.Tag = "mDn";
            this.MoveDown.UseVisualStyleBackColor = false;
            this.MoveDown.Click += new System.EventHandler(this.MoveBall);
            // 
            // MoveLeft
            // 
            this.MoveLeft.BackColor = System.Drawing.Color.White;
            this.MoveLeft.Image = ((System.Drawing.Image) (resources.GetObject("MoveLeft.Image")));
            this.MoveLeft.Location = new System.Drawing.Point(617, 165);
            this.MoveLeft.Name = "MoveLeft";
            this.MoveLeft.Size = new System.Drawing.Size(45, 45);
            this.MoveLeft.TabIndex = 8;
            this.MoveLeft.Tag = "mLt";
            this.MoveLeft.UseVisualStyleBackColor = false;
            this.MoveLeft.Click += new System.EventHandler(this.MoveBall);
            // 
            // MoveRight
            // 
            this.MoveRight.BackColor = System.Drawing.Color.White;
            this.MoveRight.Image = ((System.Drawing.Image) (resources.GetObject("MoveRight.Image")));
            this.MoveRight.Location = new System.Drawing.Point(719, 165);
            this.MoveRight.Name = "MoveRight";
            this.MoveRight.Size = new System.Drawing.Size(45, 45);
            this.MoveRight.TabIndex = 9;
            this.MoveRight.Tag = "mRt";
            this.MoveRight.UseVisualStyleBackColor = false;
            this.MoveRight.Click += new System.EventHandler(this.MoveBall);
            // 
            // ResetGame
            // 
            this.ResetGame.Location = new System.Drawing.Point(101, 413);
            this.ResetGame.Name = "ResetGame";
            this.ResetGame.Size = new System.Drawing.Size(83, 48);
            this.ResetGame.TabIndex = 10;
            this.ResetGame.Text = "Reset";
            this.ResetGame.UseVisualStyleBackColor = true;
            this.ResetGame.Click += new System.EventHandler(this.ResetGame_Click);
            // 
            // SelectControls
            // 
            this.SelectControls.Controls.Add(this.PaddleControl);
            this.SelectControls.Controls.Add(this.BallControl);
            this.SelectControls.Location = new System.Drawing.Point(502, 353);
            this.SelectControls.Name = "SelectControls";
            this.SelectControls.Size = new System.Drawing.Size(285, 122);
            this.SelectControls.TabIndex = 11;
            this.SelectControls.TabStop = false;
            this.SelectControls.Text = "Wahl der Steuerung";
            // 
            // PaddleControl
            // 
            this.PaddleControl.Checked = true;
            this.PaddleControl.Location = new System.Drawing.Point(21, 72);
            this.PaddleControl.Name = "PaddleControl";
            this.PaddleControl.Size = new System.Drawing.Size(153, 24);
            this.PaddleControl.TabIndex = 1;
            this.PaddleControl.TabStop = true;
            this.PaddleControl.Text = "Schlägersteuerung";
            this.PaddleControl.UseVisualStyleBackColor = true;
            this.PaddleControl.CheckedChanged += new System.EventHandler(this.PaddleControl_CheckedChanged);
            // 
            // BallControl
            // 
            this.BallControl.Location = new System.Drawing.Point(21, 42);
            this.BallControl.Name = "BallControl";
            this.BallControl.Size = new System.Drawing.Size(104, 24);
            this.BallControl.TabIndex = 0;
            this.BallControl.Text = "Ballsteuerung";
            this.BallControl.UseVisualStyleBackColor = true;
            this.BallControl.CheckedChanged += new System.EventHandler(this.BallControl_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(797, 487);
            this.Controls.Add(this.SelectControls);
            this.Controls.Add(this.ResetGame);
            this.Controls.Add(this.MoveRight);
            this.Controls.Add(this.MoveLeft);
            this.Controls.Add(this.MoveDown);
            this.Controls.Add(this.MoveUp);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ShowPoints);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ScrollRightPaddle);
            this.Controls.Add(this.StartGame);
            this.Controls.Add(this.GameField);
            this.Name = "Form1";
            this.Text = "Ping-Pong Spiel";
            this.Load += new System.EventHandler(this.GameWindow_Load);
            this.Click += new System.EventHandler(this.MoveBall);
            this.GameField.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.RightPaddle)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.Ball)).EndInit();
            this.SelectControls.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Panel GameField;
        private System.Windows.Forms.Button StartGame;
        private System.Windows.Forms.Timer GameTimer;
        private System.Windows.Forms.PictureBox Ball;
        private System.Windows.Forms.PictureBox RightPaddle;
        private System.Windows.Forms.VScrollBar ScrollRightPaddle;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox ShowPoints;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button MoveUp;
        private System.Windows.Forms.Button MoveDown;
        private System.Windows.Forms.Button MoveLeft;
        private System.Windows.Forms.Button MoveRight;
        private System.Windows.Forms.Button ResetGame;
        private System.Windows.Forms.GroupBox SelectControls;
        private System.Windows.Forms.RadioButton BallControl;
        private System.Windows.Forms.RadioButton PaddleControl;
    }
}