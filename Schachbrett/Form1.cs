using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Schachbrett
{
    //Basis code aus: https://stackoverflow.com/questions/18203809/c-sharp-drawing-chess-board
    public partial class Form1 : Form
    {
        private int count;
        
        
        public Form1()
        {
            InitializeComponent();
        }
        
        private void ApplyRows_Click(object sender, EventArgs e)
        { 
            //definieren wir Grösse und Farben Variablen.
            Bitmap pic = new Bitmap(600, 600);
            Graphics g = Graphics.FromImage(pic);
            count = Convert.ToInt32(SelectRowNumber.Value);
            Color color1, color2;
          //Erste Array für Farbenbestimmung. Wenn Modulo gerade ist, dann sollte color1 und 2 Schwarz und Weiss sein. Wenn ungerade, Weiss und Schwarz.
          for (int i = 0; i < count; i++)
          {
              if (i % 2 == 0)
              {
                  color1 = Color.Black;
                  color2 = Color.White;
              }
              else
              {
                  color1 = Color.White;
                  color2 = Color.Black;
              }
              SolidBrush blackBrush = new SolidBrush(color1);
              SolidBrush whiteBrush = new SolidBrush(color2);
              
              //Zweite Array für bestimmung die Farben.
              for (int j = 0; j < count; j++)
              {
                  if (j % 2 == 0)
                      g.FillRectangle(blackBrush, i * 100, j * 100, 100, 100);
                  else
                      g.FillRectangle(whiteBrush, i * 100, j * 100, 100, 100);
              }
              //Sonst zeigt den Schachbrett nicht.
              BackgroundImage = pic;
          }
        }
    }
}